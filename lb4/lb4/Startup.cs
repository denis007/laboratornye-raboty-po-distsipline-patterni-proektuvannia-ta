﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(lb4.Startup))]
namespace lb4
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
