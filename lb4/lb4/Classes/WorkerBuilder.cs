﻿using PPTIIS;

namespace PPTIIS_lb4
{
    public abstract class WorkerBuilder
    {
        public Worker worker;

        public void CreateWorker()
        {
            worker = new Worker();
        }

        public abstract void buildWorkerId();
        public abstract void buildWorkerFname();
        public abstract void buildWorkerLname();
        public abstract void buildWorkerAvgSalary();

        public Worker GetWorker()
        {
            return worker;
        }
    }
}
