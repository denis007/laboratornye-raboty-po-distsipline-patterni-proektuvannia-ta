﻿
using PPTIIS;

namespace PPTIIS_lb4
{
    class Director
    {
        WorkerBuilder workerBuilder;

        public void setWorker(WorkerBuilder builder)
        {
            this.workerBuilder = builder;
        }

        public Worker BuildWorker()
        {
            workerBuilder.CreateWorker();
            workerBuilder.buildWorkerId();
            workerBuilder.buildWorkerFname();
            workerBuilder.buildWorkerLname();
            workerBuilder.buildWorkerAvgSalary();
            Worker worker = workerBuilder.GetWorker();

            return worker;
        }
    }
}
