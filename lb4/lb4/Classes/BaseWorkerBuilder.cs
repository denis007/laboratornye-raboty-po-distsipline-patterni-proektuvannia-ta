﻿
using PPTIIS;

namespace PPTIIS_lb4
{
    public class BaseWorkerBuilder:WorkerBuilder
    {
        Worker worker;
        public override void buildWorkerId()
        {
            worker.WorkerId = 1;
        }

        public override void buildWorkerFname()
        {
            worker.FName = "Denis";
        }

        public override void buildWorkerLname()
        {
            worker.LName = "Vasilev";
        }

        public override void buildWorkerAvgSalary()
        {
            worker.AverageSalary = 15.0;
        }
    }
}
