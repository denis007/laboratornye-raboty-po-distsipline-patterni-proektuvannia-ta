﻿
namespace PPTIIS
{
    public class Worker:IAvgRate
    {
        public int WorkerId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public double AverageSalary { get; set; }

        public double GetAverageSalary()
        {
            return AverageSalary;
        }
    }
}
