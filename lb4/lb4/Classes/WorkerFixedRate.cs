﻿
namespace PPTIIS
{
    public class WorkerFixedRate:Worker
    {
        public double FixedRate { get; set; }

        protected void GetAverageSalary()
        {
            AverageSalary = FixedRate;
        }
        public WorkerFixedRate(int id, string fname, string lname, double hourlerate)
        {
            WorkerId = id;
            FName = fname;
            LName = lname;
            FixedRate = hourlerate;
            GetAverageSalary();
        }
    }
}
