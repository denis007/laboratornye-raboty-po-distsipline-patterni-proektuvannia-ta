﻿
namespace PPTIIS
{
    class WorkerFixedRate:Worker
    {
        public double FixedRate { get; set; }

        protected void GetAverageSalary(double fixedRate)
        {
            AverageSalary = fixedRate;
        }
        public WorkerFixedRate(int id, string fname, string lname, double hourlerate)
        {
            WorkerId = id;
            FName = fname;
            LName = lname;
            GetAverageSalary(hourlerate);
        }
    }
}
