﻿
namespace PPTIIS
{
    class Worker:IAvgRate
    {
        public int WorkerId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public double AverageSalary { get; set; }

        /*protected Worker(int id, string fname, string lname, double hourlerate)
        {
            WorkerId = id;
            FName = fname;
            LName = lname;
        }*/

        public double GetAverageSalary()
        {
            return AverageSalary;
        }
    }
}
