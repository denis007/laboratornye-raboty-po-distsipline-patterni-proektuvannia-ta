﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PPTIIS;

namespace PPTIIS_lb2
{
    class ProxyWorkerFixed:Worker
    {
        private WorkerFixedRate proxyWorker;
        public double FixedRate { get; set; }

        public ProxyWorkerFixed(int id, string fname, string lname, double hourlerate)
        {
            WorkerId = id;
            FName = fname;
            LName = lname;
            FixedRate = hourlerate;
        }

        protected void GetAverageSalary()
        {
            AverageSalary = FixedRate;
        }

        public void GetInfo()
        {
            if (proxyWorker == null)
            {
                GetAverageSalary();
                proxyWorker = new WorkerFixedRate(WorkerId, FName, LName, FixedRate);
            }            
        }
    }
}
