﻿
namespace PPTIIS
{
    public class WorkerPerHour:Worker
    {
        public double HourlyRate { get; set; }
        protected void GetAverageSalary()
        {
            AverageSalary = 5 * 4*8*HourlyRate;
        }

        public WorkerPerHour(int id, string fname, string lname, double hourlerate)
        {
            WorkerId = id;
            FName = fname;
            LName = lname;
            HourlyRate = hourlerate;
            GetAverageSalary();
        }
    }
}
