﻿
using PPTIIS_lb5;

namespace PPTIIS
{
    public class Worker:IAvgRate
    {
        public IActivity Activity { get; set; }
        public int WorkerId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public double AverageSalary { get; set; }

        public void ExecuteActivity()
        {
            Activity.DoIt();
        }

        public double GetAverageSalary()
        {
            return AverageSalary;
        }
    }
}
