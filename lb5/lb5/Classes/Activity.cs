﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPTIIS_lb5
{
    public interface IActivity
    {
        void DoIt();
    }
}
