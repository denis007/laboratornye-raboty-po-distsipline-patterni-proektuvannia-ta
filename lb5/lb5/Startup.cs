﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(lb5.Startup))]
namespace lb5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
