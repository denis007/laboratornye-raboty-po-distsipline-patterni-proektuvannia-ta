﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PPTIIS;

namespace PPTIIS_lb3
{
    class Adapter:Worker, IData
    {
        public void SaveData(List<string> list)
        {
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"D:\text.txt"))
            {
                foreach (string line in list)
                {
                    file.WriteLine(line);
                }
            }
            LoadData(list);
        }
    }
}
