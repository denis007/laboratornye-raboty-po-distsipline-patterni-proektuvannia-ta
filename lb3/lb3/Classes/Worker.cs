﻿
using System;
using System.Collections;
using System.Collections.Generic;
using PPTIIS_lb3;

namespace PPTIIS
{
    class Worker:IAvgRate
    {
        public int WorkerId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public double AverageSalary { get; set; }

        public double GetAverageSalary()
        {
            return AverageSalary;
        }

        protected void LoadData(List<string> list)
        {
            string[] lines = System.IO.File.ReadAllLines(@"D:\text.txt");

            Console.WriteLine("Contents of text.txt file = \n");
            foreach (string line in lines)
            {
                Console.WriteLine("\t" + line);
            }
        }
    }
}
