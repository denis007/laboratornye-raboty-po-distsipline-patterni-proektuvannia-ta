﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Models;

namespace BL
{
    public class WorkerController
    {
        #region Singleton

        private static WorkerController _instance;

        private WorkerController()
        {
            _workerList = new List<BaseWorker>();
        }

        public static WorkerController GetInstance()
        {
            _instance = _instance ?? new WorkerController();
            return _instance;
        }
        #endregion

        private List<BaseWorker> _workerList;


        public void AddWorker(BaseWorker worker) => _workerList.Add(worker);

        public List<BaseWorker> GetWorkers() => _workerList;

        public List<BaseWorker> GetSortedWorkers() => _workerList.OrderByDescending(_ => _.Salary).ToList();
    }
}
