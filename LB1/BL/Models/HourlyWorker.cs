﻿
namespace BL.Models
{
    public class HourlyWorker : BaseWorker
    {
        private readonly double _salary = 0;

        public HourlyWorker(string id, string name, double payment)
        {
            Id = id;
            Name = name;
            _salary = 5 * 4 * 8*payment;
        }

        public override double Salary
        {
            get { return _salary; }
            set { value = _salary; }
        }
    }
}
