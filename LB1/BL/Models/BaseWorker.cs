﻿namespace BL.Models
{
    public abstract class BaseWorker
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public abstract double Salary { get; set; }

    }
}
