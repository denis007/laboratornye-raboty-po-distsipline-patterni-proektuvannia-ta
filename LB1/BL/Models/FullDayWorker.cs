﻿
namespace BL.Models
{
    public class FullDayWorker : BaseWorker
    {
        public FullDayWorker(string id, string name, double payment)
        {
            Id = id;
            Name = name;
            Salary = payment;
        }

        public override double Salary { get; set; }
    }
}
