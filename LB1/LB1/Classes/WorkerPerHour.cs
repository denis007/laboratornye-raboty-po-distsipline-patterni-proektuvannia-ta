﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPTIIS
{
    class WorkerPerHour:Worker
    {
        public double HourlyRate { get; set; }
        protected void GetAverageSalary()
        {
            AverageSalary = 5 * 4*8*HourlyRate;
        }

        public WorkerPerHour(int id, string fname, string lname, double hourlerate)
        {
            WorkerId = id;
            FName = fname;
            LName = lname;
            HourlyRate = hourlerate;
            GetAverageSalary();
        }
    }
}
