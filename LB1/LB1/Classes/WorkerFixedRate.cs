﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPTIIS
{
    class WorkerFixedRate:Worker
    {

        protected void GetAverageSalary(double fixedRate)
        {
            AverageSalary = fixedRate;
        }
        public WorkerFixedRate(int id, string fname, string lname, double hourlerate)
        {
            WorkerId = id;
            FName = fname;
            LName = lname;
            GetAverageSalary(hourlerate);
        }
    }
}
