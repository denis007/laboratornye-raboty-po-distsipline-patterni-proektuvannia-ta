﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPTIIS
{
    class Worker:IAvgRate
    {
        public int WorkerId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public double AverageSalary { get; set; }

        public double GetAverageSalary()
        {
            return AverageSalary;
        }
    }
}
