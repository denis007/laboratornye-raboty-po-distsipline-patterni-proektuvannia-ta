﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using PPTIIS;

namespace lb1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Worker> workers = new List<Worker>();
        public MainWindow()
        {
            InitializeComponent();            
            workers.Add(new WorkerPerHour(1, "Denis", "Vasilev", 20.0));
            workers.Add(new WorkerPerHour(2, "Denis", "Vasilev", 35.0));
            workers.Add(new WorkerPerHour(3, "Denis", "Vasilev", 40.0));
            workers.Add(new WorkerPerHour(4, "Denis", "Vasilev", 56.0));
            workers.Add(new WorkerPerHour(5, "Denis", "Vasilev", 30.0));
            workers.Add(new WorkerFixedRate(6, "Denis", "Vasilev", 78321.0));
            workers.Add(new WorkerFixedRate(7, "Denis", "Vasilev", 7895.0));
            workers.Add(new WorkerFixedRate(8, "Denis", "Vasilev", 2950.0));
            workers.Add(new WorkerFixedRate(9, "Denis", "Vasilev", 5560.0));
            workers.Add(new WorkerFixedRate(10, "Denis", "Vasilev", 5000.0));
            DataGrid.DataContext = workers;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<Worker> query = workers.OrderBy(worker => worker.AverageSalary).Take(5);
            query.ToArray();
            DataGrid.DataContext = query;
            List<string> list = new List<string>();
            foreach (var worker in query)
            {
                list.Add(worker.WorkerId + " - " + worker.FName + " " + worker.LName + " : " + worker.AverageSalary);
            }

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"D:\text.txt"))
            {
                foreach (string line in list)
                {
                    file.WriteLine(line);
                }
            }

            string[] lines = System.IO.File.ReadAllLines(@"D:\text.txt");

            Console.WriteLine("Contents of text.txt file = \n");
            foreach (string line in lines)
            {
                Console.WriteLine("\t" + line);
            }
        }
    }
}