﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using BL;
using BL.Models;
using LB1.Command;

namespace LB1.ViewModels
{
    public class WorkerViewModel : BaseViewModel
    {
        private WorkerController _controller;
        private ObservableCollection<BaseWorker> _workerList;

        public string WorkerName { get; set; }
        public double Payment { get; set; }

        public ObservableCollection<BaseWorker> WorkerList
        {
            get { return _workerList; }
            set
            {
                _workerList = value;
                OnPropertyChanged(nameof(WorkerList));
            }
        }

        public ICommand AddWorker { get; set; }

        public WorkerViewModel()
        {
            _controller = WorkerController.GetInstance();
            WorkerName = "";
            Payment = 0;

            WorkerList = new ObservableCollection<BaseWorker>();

            AddWorker = new RelayCommand(AddWorkerAction);
        }

        public void AddWorkerAction(object obj)
        {
            var id = int.Parse(obj as string);

            switch (id)
            {
                case 0:
                    _controller.AddWorker(new FullDayWorker(Guid.NewGuid().ToString(), WorkerName, Payment));
                    break;
                case 1:
                    _controller.AddWorker(new HourlyWorker(Guid.NewGuid().ToString(), WorkerName, Payment));
                    break;
            }

            WorkerList.Clear();
            foreach (var w in _controller.GetSortedWorkers())
            {
                WorkerList.Add(w);
            }
        }


    }
}
